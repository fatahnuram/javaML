package javaML;

public class Backprop{
	// variable declarations
	private DatasetModel[] datasetModel;
	private double[][] hiddenLayer;
	private double[] outputLayer;
	private double totalError;

	// constructor
	public Backprop(DatasetModel[] datasetModel, int hiddenLayer, int neuronPerHiddenLayer, int outputNeuron){
		this.datasetModel = datasetModel;
		this.hiddenLayer = new double[hiddenLayer][neuronPerHiddenLayer];
		this.outputLayer = new double[outputNeuron];
	}

	// method for count weighted sum of model and hidden layer
	private double countWeightedSum(double[] inputList, double[] weightList){
		// init local variable
		double sum = 0;

		// loop through all the weight and input
		for(int i=0; i<inputList.length; i++){
			// multiply input and weight
			sum += weightList[i]*inputList[i];
		}

		// return the weighted sum
		return sum;
	}

	// method for count activation function
	private double countSigmoid(double val){
		// return the result of sigmoid function
		return 1/(1+Math.exp(-val));
	}

	// method for count forward pass
	private double countForwardPass(double[] inputList, double[] weightList){
		// calculate the weighted sum
		double sum = countWeightedSum(inputList, weightList);
		// return value after calculating sigmoid function
		return countSigmoid(sum);
	}

	// method for calculating the error
	private double[] calculateError(double[] actualValue, double[] targetValue){
		// init global variable
		totalError = 0;

		// init local variable for temporary
		double[] errors = new double[actualValue.length];
		for(double error : errors){
			error = 0;
		}

		// calculate for all output neuron
		for(int i=0; i<actualValue.length; i++){
			// calculate error
			errors[i] = (targetValue[i]-actualValue[i])*(targetValue[i]-actualValue[i])/2;
			// sum up the errors to total error
			totalError += errors[i];
		}

		// return the error values
		return errors;
	}
}