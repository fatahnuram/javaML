package javaML;

import java.io.*;

public class DatasetReader {
	// variable declarations
	private String content;
	private String[] contentArray;
	private String[][] contentMatrix;
	private DatasetModel[] contentModel;
	private File file;
	private FileReader fileReader;
	private BufferedReader bufferedReader;
	private StringBuilder builder;
	private String line;

	// constructor
	public DatasetReader(String path) throws Exception{
		// init empty string for container content
		this.content = "";
		// get the file to be read
		file = new File(path);

		try{
			// init file reader for file
			fileReader = new FileReader(file);
			// init buffered reader for file reader
			bufferedReader = new BufferedReader(fileReader);

			// init string builder
			builder = new StringBuilder();

			// read first line
			line = bufferedReader.readLine();
			// loop through the whole file
			while(line != null){
				// append per line to builder
				builder.append(line);
				// append the line separator to builder
				builder.append(System.lineSeparator());
				// read next line
				line = bufferedReader.readLine();
			}

			// make string builder into string
			content = builder.toString();

		}catch(Exception e){
			// show error to screen
			e.printStackTrace();
		}finally{
			// close the connection
			bufferedReader.close();
		}

		// parse content from string as an array of string
		contentArray = parseContentArray(content);
		// parse content from string as an matrix
		contentMatrix = parseContentMatrix(contentArray);
		// parse content from matrix as a model
		contentModel = parseContentModel(contentMatrix);
	}

	// method for parsing content to array
	private String[] parseContentArray(String content){
		// split the content to array
		contentArray = content.split(System.lineSeparator());
		return contentArray;
	}

	// method for parsing content to matrix
	private String[][] parseContentMatrix(String[] contentArray){
		// init content matrix
		contentMatrix = new String[contentArray.length][];

		// loop through all the data in array
		for(int i=0; i<contentArray.length; i++){
			// split per item in array into new array --> matrix
			contentMatrix[i] = contentArray[i].split(",");
		}

		return contentMatrix;
	}

	// method for parsing content to model
	private DatasetModel[] parseContentModel(String[][] contentMatrix){
		// init models
		contentModel = new DatasetModel[contentMatrix.length];
		// init local variable for container
		double[] feature = new double[contentMatrix[0].length - 1];
		// int[] classifier = new int[2];
		int classifier = -1;

		// loop through all the data
		for(int i=0; i<contentMatrix.length; i++){
			// loop through all feature in a data
			for(int j=0; j<contentMatrix[i].length-1; j++){
				// parse from string to double for feature
				feature[j] = Double.parseDouble(contentMatrix[i][j]);
				// parse from string to int for classifier
			}

			switch(contentMatrix[i][4]){
				case "Iris-setosa":
					classifier = 1;
					break;
				case "Iris-versicolor":
					classifier = 2;
					break;
				case "Iris-virginica":
					classifier = 3;
					break;
				default:
					classifier = 0;
					break;
			}

			// switch(contentMatrix[i][4]){
			// 	case "Iris-setosa":
			// 		classifier[0] = 0;
			// 		classifier[1] = 1;
			// 		break;
			// 	case "Iris-versicolor":
			// 		classifier[0] = 1;
			// 		classifier[1] = 0;
			// 		break;
			// 	case "Iris-virginica":
			// 		classifier[0] = classifier[1] = 1;
			// 		break;
			// 	default:
			// 		classifier[0] = classifier[1] = 0;
			// 		break;
			// }

			// set the feature values into model
			contentModel[i].setFeature(feature);
			// set the classifier class into model
			contentModel[i].setClassifier(classifier);
		}

		return contentModel;
	}

	// method for getting the content
	public String getContent(){
		return content;
	}

	// method for getting content as array
	public String[] getContentArray(){
		return contentArray;
	}

	// method for getting content as matrix
	public String[][] getContentMatrix(){
		return contentMatrix;
	}

	// method for getting content as array of model
	public DatasetModel[] getContentModel(){
		return contentModel;
	}
}