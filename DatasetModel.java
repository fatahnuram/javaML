package javaML;

public class DatasetModel{
	// variable declarations
	protected double[] feature; // feature
	protected int classifier; // classification
	// classifier value:
	// 0 --> default
	// -1 --> will be processed
	// n --> output class
	protected String[] classList; // class list in array

	// constructor
	public DatasetModel(String[] classList){
		// get data
		this.classList = classList;
	}

	// setter
	public void setFeature(double[] feature){
		this.feature = feature;
	}

	public void setClassifier(int classifier){
		this.classifier = classifier;
	}

	// getter
	public double[] getFeature(){
		return feature;
	}

	public int getClassifier(){
		return classifier;
	}

	public String[] getClassList(){
		return classList;
	}
}